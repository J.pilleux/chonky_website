all:
	docker compose up --build -d

stop:
	docker compose down

update:
	git submodule foreach git pull

